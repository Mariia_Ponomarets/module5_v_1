var pokemonApp = angular.module('pokemonApp', ['ngRoute']);
pokemonApp.controller('navCntrl', function ($scope) {
  $scope.collapseMenu = function () {
    document.getElementsByClassName('topnav')[0].classList.toggle('responsive');
  };
})
pokemonApp.controller('listCntrl', function ($scope, $http) {
  var listContainer = document.getElementById('list-container');
  var elemContainer = document.getElementById('elem-container');
  $scope.content = [];
  $scope.selectedItems;
  $scope.flagShowItems = false;
  $scope.offset = 12;
  $scope.arrTypes = [];
  $scope.contentType = [];
  $scope.idBefore = '';
  $scope.sort = '';
  $scope.getElementFromServer = function (url) {
    $http.get(url)
    .then(function (response) {
      $scope.content = response.data;
      for (var i = 0; i < $scope.content.objects.length; i++) {
        var tmp = $scope.content.objects[i];
        delete tmp.descriptions;
        delete tmp.egg_groups;
        delete tmp.moves;
        delete tmp.sprites;

        $scope.content.objects[i] = tmp;
      }
    }, function (response) {
      console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext);
    })
  };
  $scope.getElementFromServer('http://pokeapi.co/api/v1/pokemon/?limit=12');
  $scope.checkFamousElem = function (id) {
    var key = 'pokemon-'+id;
    if (localStorage.getItem(key) !== null) {
      var elId = 'icon' + localStorage.getItem(key);
    document.getElementById(elId).classList.add('like-icon-active');
    }
  };

  $scope.pokemonClick = function (obj) {
    var id = 'container' + obj.national_id;
    var classListContainer = document.getElementsByClassName('border');
    for (var i = 0; i < classListContainer.length; i++) {
      if (classListContainer[i].classList.contains('active')) {
        classListContainer[i].classList.remove('active')
      }
    }
    document.getElementById(id).classList.add('active');
  }
  $scope.loadMoreElements = function () {
    $scope.offset += 12;
    var urlServer = 'http://pokeapi.co/api/v1/pokemon/?limit=' + $scope.offset;
    $scope.getElementFromServer(urlServer);
  }
  $scope.showInfo = function (obj, status) {
    $scope.selectedItems = obj;
    $scope.flagShowItems = status;
    var id = 'container' + obj.national_id;
    $scope.pokemonClick(obj);
    listContainer.classList.remove('list');
    listContainer.classList.add('list-elem');
  }
  $scope.hideInfo = function (status) {
    $scope.flagShowItems = status;
     listContainer.classList.remove('list-elem');
    listContainer.classList.add('list');

        var classListContainer = document.getElementsByClassName('border');
    for (var i = 0; i < classListContainer.length; i++) {
      if (classListContainer[i].classList.contains('active')) {
        classListContainer[i].classList.remove('active')
      }
    }
  };
  $scope.saveToLocalStorage = function (obj) {
    var key = 'pokemon-' + obj.name;
    var id = 'icon' + obj.national_id;
    if (document.getElementById(id).classList.contains('like-icon-active')) {
      localStorage.removeItem(key);
      document.getElementById(id).classList.remove('like-icon-active')
    }
    else {
      localStorage.setItem(key, obj.national_id);
      document.getElementById(id).classList.add('like-icon-active')
    }
  };
  $scope.getAllType = function () {
    $http.get('http://pokeapi.co/api/v1/type/?limit=999')
        .then(function (response) {
          var tmp = [];
          $scope.contentType = response.data;
          for (var i = 0; i < $scope.contentType.objects.length; i++) {
            tmp[i] = $scope.contentType.objects[i].name;
          }
          $scope.arrTypes = unique(tmp);
        }, function (response) {
          console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext);
        })
  };

  $scope.showSelectedElem = function (item) {
    $scope.sort = '';
    $scope.sort = item;
  }
  $scope.getAllType();
  function unique(arr) {
    var result = [];
    nextInput:
      for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        for (var j = 0; j < result.length; j++) {
          if (result[j] == str) continue nextInput;
        }
        result.push(str);
      }
    return result;
  }
});


pokemonApp.controller('famouslistCntrl', function ($scope, $http) {
  $scope.contentFromLocalStorage = [];
  $scope.endNum = 12;
  $scope.flagShowFamousItems = false;
  $scope.selectedFamousItems;
  $scope.sortFamous;
  $scope.arrTypesFamous = [];
  $scope.selectedItems;
  $scope.contentTypeFamous = [];
  var listContainer = document.getElementById('list-container');
  var elemContainer = document.getElementById('elem-container');
  function loadFromLocalStorage() {
    var dataId = [];
    var tmpArr = [];
    for (key in window.localStorage) {
      if (key.indexOf('pokemon') === 1 || key.indexOf('pokemon') === 0) {
        var tmp = localStorage.getItem(key);
        dataId.push(tmp);
      }
    }

    for (var i = 0; i < dataId.length; i++) {
      var url = 'http://pokeapi.co/api/v1/pokemon/' + dataId[i];
      $http.get(url).then(function (response) {

        tmpArr.push(response.data);
        delete tmpArr.descriptions;
        delete tmpArr.egg_groups;
        delete tmpArr.moves;
        delete tmpArr.sprites;

      }, function (response) {
        console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext);
      })
    }
    $scope.contentFromLocalStorage = tmpArr;
  };
  loadFromLocalStorage();

  $scope.clickFamousPokemon = function (obj) {
    var id = 'containerFamous' + obj.national_id;
    var classListContainer = document.getElementsByClassName('border');
    for (var i = 0; i < classListContainer.length; i++) {
      if (classListContainer[i].classList.contains('active')) {
        classListContainer[i].classList.remove('active')
      }
    }
    listContainer.classList.remove('list');
    listContainer.classList.add('list-elem');
    document.getElementById(id).classList.add('active');
  }
  $scope.loadMoreFromLocalStorage = function () {
    if (($scope.endNum + 12) > window.localStorage.length) {
      $scope.endNum = window.localStorage.length;
    }
    else {
      $scope.endNum += 12;
    }
  }
  $scope.showFamousItems = function (obj, status) {
    $scope.selectedFamousItems = obj;
    $scope.flagShowFamousItems = status;
    $scope.clickFamousPokemon(obj);
    listContainer.classList.remove('list');
    listContainer.classList.add('list-elem');
  }
  $scope.hideFamousItem = function (status) {
    $scope.flagShowFamousItems = status;
 listContainer.classList.remove('list-elem');
    listContainer.classList.add('list');
    var classListContainer = document.getElementsByClassName('border');
    for (var i = 0; i < classListContainer.length; i++) {
      if (classListContainer[i].classList.contains('active')) {
        classListContainer[i].classList.remove('active')
      }
    }
  }
  $scope.deleteFromLocalStorage = function (obj) {
    var key = 'pokemon-' + obj.name;
    for (var i = 0; i < $scope.contentFromLocalStorage.length; i++) {
      if (obj.name === $scope.contentFromLocalStorage[i].name) {

        localStorage.removeItem(key);
        loadFromLocalStorage();
      }
    }

  };
  $scope.showFamousSelectedElem = function (item) {
    $scope.sortFamous = '';
    $scope.sortFamous = item;
  };
  $scope.getAllTypesFamous = function () {
    $http.get('http://pokeapi.co/api/v1/type/?limit=999')
        .then(function (response) {
          var tmp = [];
          $scope.contentTypeFamous = response.data;
          for (var i = 0; i < $scope.contentTypeFamous.objects.length; i++) {
            tmp[i] = $scope.contentTypeFamous.objects[i].name;
          }
          $scope.arrTypesFamous = $scope.uniqueFamous(tmp);
        }, function (response) {
          console.log('Houston we have a problem' + ' statuscode:' + response.status + ', statustext:' + response.statustext);
        });
  };
  $scope.getAllTypesFamous();

  $scope.uniqueFamous = function (arr) {
    var result = [];
    nextInput:
      for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        for (var j = 0; j < result.length; j++) {
          if (result[j] == str) continue nextInput;
        }
        result.push(str);
      }
    return result;
  }

});

pokemonApp.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.
    when('/home', {
      templateUrl: 'views/allPokemons.html',
      controller: 'listCntrl'
    }).
  when('/famous', {
    templateUrl: 'views/famousPokemons.html',
    controller: 'famouslistCntrl'
  }).
  otherwise({
    redirectTo: '/home'
  });
}]);